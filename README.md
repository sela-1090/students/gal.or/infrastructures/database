# Database
# Installing PostgreSQL with Helm

## Connect to the AKS  & Do all the commands later in the AKS
~~~
az login
~~~
~~~
az account set --subscription <subscription ID>
~~~
~~~
az aks get-credentials --resource-group <RESOURCE_GROUP_NAME> --name <AKS_CLUSTER_NAME>
~~~

## Create a Namespace

If the desired namespace doesn't exist, create it using the following command:

~~~
kubectl create namespace database
~~~

## Add Bitnami Helm Repository

If you haven't added the Bitnami Helm repository before, you need to do it once:

~~~
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
~~~

## Install PostgreSQL

Run the following Helm command to install PostgreSQL using the Bitnami Helm chart:

~~~
helm install my-release -n database oci://registry-1.docker.io/bitnamicharts/postgresql
~~~

## Retrieve PostgreSQL Password

After the installation is complete, you can retrieve the PostgreSQL password using the following command:

~~~
export POSTGRES_PASSWORD=$(kubectl get secret --namespace database my-release-postgresql -o jsonpath="{.data.postgres-password}" | base64 -d)
echo $POSTGRES_PASSWORD
~~~

## Connect to PostgreSQL

Use the following commands to connect to the PostgreSQL instance using the PostgreSQL client:

~~~
kubectl run my-release-postgresql-client --rm --tty -i --restart='Never' --namespace database --image docker.io/bitnami/postgresql:15.3.0-debian-11-r77 --env="PGPASSWORD=$POSTGRES_PASSWORD" \
  --command -- psql --host my-release-postgresql -U postgres -d postgres -p 5432
~~~

This will open an interactive PostgreSQL session.

## Cleanup

If you want to uninstall the PostgreSQL instance, you can use the following Helm command:

~~~
helm uninstall my-release -n database
~~~

## Conclusion

You've successfully installed PostgreSQL using Helm and connected to it using the PostgreSQL client. Helm makes it easier to manage and deploy complex applications like databases on Kubernetes clusters.

For more information and customization options, you can refer to the [Bitnami PostgreSQL Helm Chart documentation](https://bitnami.com/stack/postgresql/helm).
